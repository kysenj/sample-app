import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Loader } from 'components/common';
import store, { history } from './store';

const Routes = React.lazy(() => import(/* webpackChunkName: "routs" */ './routes/Routes'));

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Suspense fallback={<Loader />}>
        <Routes />
      </Suspense>
    </ConnectedRouter>
  </Provider>
);

export default App;
