import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col } from 'reactstrap';
import { isEmpty, union } from 'lodash';
import { fetchDocuments } from 'store/documents/actions';
import { getLoadingSelector, getDocumentsSelector } from 'store/documents/selectors';
import { DocumentSearch, DocumentTopics, DocumentCard, DocumentPanel } from 'components/documents';
import { PageTitle, Loader, EmptyCard } from 'components/common';

const Documents = () => {
  // States
  const [searchKey, setSearchKey] = useState('');
  const [selectedTopicIds, setSelectedTopicIds] = useState([]);
  const [selectedDocument, setSelectedDocument] = useState(null);

  // Dispather
  const dispatch = useDispatch();

  // Documents
  const loading = useSelector(getLoadingSelector);
  const documents = useSelector(getDocumentsSelector);

  useEffect(() => {
    dispatch(fetchDocuments());
  }, [dispatch]);

  const getFilteredDocuments = () =>
    documents.filter((document) => {
      let filterable = true;

      if (!isEmpty(searchKey)) {
        filterable = filterable && document.title.toLowerCase().includes(searchKey.toLowerCase());
      }

      if (!isEmpty(selectedTopicIds)) {
        filterable =
          filterable && selectedTopicIds.every((topicId) => document.topics.map(({ id }) => id).includes(topicId));
      }

      return filterable;
    });

  const togglePanelPopup = () => {
    document.querySelector('.document-panel1').classList.toggle('pop-up');
  };

  const onSearchDocuments = (e) => {
    if (e.key === 'Enter') {
      setSearchKey(e.target.value);
      setSelectedDocument(null);
    }
  };

  const onSelectDocument = (document) => {
    setSelectedDocument(document);
    togglePanelPopup();
  };

  const onSelectTopic = (e, topicId) => {
    e.stopPropagation();
    setSelectedTopicIds(union([...selectedTopicIds, topicId]));
  };

  const onDeselectTopic = (topicId) => {
    setSelectedTopicIds(selectedTopicIds.filter((id) => id !== topicId));
  };

  const renderDocumentItems = () => {
    const filteredDocuments = getFilteredDocuments();

    return !isEmpty(filteredDocuments) ? (
      filteredDocuments.map((document) => (
        <DocumentCard
          key={document.id}
          document={document}
          selectedTopicIds={selectedTopicIds}
          onSelectDocument={onSelectDocument}
          onSelectTopic={onSelectTopic}
        />
      ))
    ) : (
      <EmptyCard title="No Documents To Show!" />
    );
  };

  return (
    <>
      <PageTitle title="Testing Company AI:: Welcome to visit the Testing Company Testing Application" />
      <div className="documents">
        <div className="documents-search-container">
          <Row>
            <Col md="6" xl="4">
              <DocumentSearch searchKey={searchKey} onSearchChanged={onSearchDocuments} />
            </Col>
            <Col md="6" xl="8">
              <DocumentTopics selectedTopicIds={selectedTopicIds} onDeselectTopic={onDeselectTopic} />
            </Col>
          </Row>
        </div>
        <div className="documents-content-container container-fluid">
          {loading ? (
            <Loader />
          ) : (
            <Row>
              <Col md="6" xl="5" className="document-card-container">
                {renderDocumentItems()}
              </Col>
              <Col md="6" xl="7" className="document-panel-container">
                <DocumentPanel document={selectedDocument} toggle={togglePanelPopup} />
              </Col>
            </Row>
          )}
        </div>
      </div>
    </>
  );
};

export default Documents;
