import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { PageTitle } from 'components/common';

const NotFound = () => (
  <>
    <PageTitle title="Testing Company AI:: Welcome to visit the Testing Company Testing Application" />
    <Container className="text-center p-5">
      <Row>
        <Col xs="12">
          <h1 className="text-danger">404</h1>
          <h3>OOPS!</h3>
          <h4 className="text-info">Page Not Found</h4>
        </Col>
      </Row>
      <Row>
        <Col xs="12">
          <Link to="/" className="text-center">
            Back To Home
          </Link>
        </Col>
      </Row>
    </Container>
  </>
);

export default NotFound;
