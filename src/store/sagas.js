import { all, fork } from '@redux-saga/core/effects';
import documentsSaga from './documents/saga';
import topicsSaga from './topics/saga';

/**
 * Main saga
 */
export function* rootSaga() {
  yield all([fork(documentsSaga), fork(topicsSaga)]);
}
