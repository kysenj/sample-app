import { FETCH_TOPICS_REQUEST, FETCH_TOPICS_SUCCESS, FETCH_TOPICS_FAILURE } from './actionTypes';

/**
 * Fetch topics, this action starts the request saga
 *
 * @return {object} An action object with a type of FETCH_TOPICS_REQUEST
 */
export const fetchTopics = () => {
  return {
    type: FETCH_TOPICS_REQUEST,
  };
};

/**
 * Dispatched when the topics are fetched by the request saga
 *
 * @param  {object} topics topics info
 *
 * @return {object} An action object with a type of FETCH_TOPICS_SUCCESS passing the topics
 */
export const fetchTopicsSucceess = (data) => {
  return {
    type: FETCH_TOPICS_SUCCESS,
    payload: { result: data },
  };
};

/**
 * Dispatched when fetching topics fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of FETCH_TOPICS_FAILURE passing the error
 */
export const fetchTopicsFailure = (error) => {
  return {
    type: FETCH_TOPICS_FAILURE,
    payload: { error },
  };
};
