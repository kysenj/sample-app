import { createSelector } from 'reselect';

const getLoading = (state) => state.topics.loading;
const getTopics = (state) => state.topics.data;
const getError = (state) => state.topics.error;

export const getLoadingSelector = createSelector(getLoading, (loading) => loading);
export const getTopicsSelector = createSelector(getTopics, (topics) => topics);
export const getErrorSelector = createSelector(getError, (error) => error);
