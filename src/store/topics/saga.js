import { all, call, put, takeLatest } from '@redux-saga/core/effects';
import { topics } from 'data/topics';
import { fetchTopicsFailure, fetchTopicsSucceess } from './actions';
import { FETCH_TOPICS_REQUEST } from './actionTypes';

const getTopics = () => topics;

/*
  Worker Saga: Fired on FETCH_TOPICS_REQUEST action
*/
function* fetchTopicssSaga() {
  try {
    const data = yield call(getTopics);
    yield put(fetchTopicsSucceess({ topics: data }));
  } catch (e) {
    yield put(fetchTopicsFailure({ error: e.message }));
  }
}

/*
  Starts worker saga on latest dispatched `FETCH_TOPICS_REQUEST` action.
  Allows concurrent increments.
*/
function* topicsSaga() {
  yield all([takeLatest(FETCH_TOPICS_REQUEST, fetchTopicssSaga)]);
}

export default topicsSaga;
