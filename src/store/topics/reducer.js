import { FETCH_TOPICS_REQUEST, FETCH_TOPICS_SUCCESS, FETCH_TOPICS_FAILURE } from './actionTypes';

const initialState = {
  loading: false,
  data: [],
  error: null,
};

export default (state = initialState, action) => {
  const { error, result = {} } = action.payload || {};

  switch (action.type) {
    case FETCH_TOPICS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_TOPICS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        data: result.topics,
      };
    case FETCH_TOPICS_FAILURE:
      return {
        ...state,
        loading: false,
        error,
      };
    default:
      return {
        ...state,
      };
  }
};
