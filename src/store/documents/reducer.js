import { FETCH_DOCUMENTS_REQUEST, FETCH_DOCUMENTS_SUCCESS, FETCH_DOCUMENTS_FAILURE } from './actionTypes';

const initialState = {
  loading: false,
  data: [],
  error: null,
};

export default (state = initialState, action) => {
  const { error, result = {} } = action.payload || {};

  switch (action.type) {
    case FETCH_DOCUMENTS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_DOCUMENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        data: result.documents,
      };
    case FETCH_DOCUMENTS_FAILURE:
      return {
        ...state,
        loading: false,
        error,
      };
    default:
      return {
        ...state,
      };
  }
};
