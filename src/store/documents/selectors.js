import { createSelector } from 'reselect';

const getLoading = (state) => state.documents.loading;
const getDocuments = (state) => state.documents.data;
const getError = (state) => state.documents.error;

export const getLoadingSelector = createSelector(getLoading, (loading) => loading);
export const getDocumentsSelector = createSelector(getDocuments, (documents) => documents);
export const getErrorSelector = createSelector(getError, (error) => error);
