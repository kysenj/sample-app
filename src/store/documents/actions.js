import { FETCH_DOCUMENTS_REQUEST, FETCH_DOCUMENTS_SUCCESS, FETCH_DOCUMENTS_FAILURE } from './actionTypes';

/**
 * Fetch documents, this action starts the request saga
 *
 * @return {object} An action object with a type of FETCH_DOCUMENTS_REQUEST
 */
export const fetchDocuments = () => {
  return {
    type: FETCH_DOCUMENTS_REQUEST,
  };
};

/**
 * Dispatched when the documents are fetched by the request saga
 *
 * @param  {object} documents documents info
 *
 * @return {object} An action object with a type of FETCH_DOCUMENTS_SUCCESS passing the documents
 */
export const fetchDocumentsSucceess = (data) => {
  return {
    type: FETCH_DOCUMENTS_SUCCESS,
    payload: { result: data },
  };
};

/**
 * Dispatched when fetching documents fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of FETCH_DOCUMENTS_FAILURE passing the error
 */
export const fetchDocumentsFailure = (error) => {
  return {
    type: FETCH_DOCUMENTS_FAILURE,
    payload: { error },
  };
};
