import { all, call, put, takeLatest } from '@redux-saga/core/effects';
import { documents } from 'data/documents';
import { fetchDocumentsFailure, fetchDocumentsSucceess } from './actions';
import { FETCH_DOCUMENTS_REQUEST } from './actionTypes';

const getDocuments = () => documents;

/*
  Worker Saga: Fired on FETCH_DOCUMENTS_REQUEST action
*/
function* fetchDocumentsSaga() {
  try {
    const data = yield call(getDocuments);
    yield put(fetchDocumentsSucceess({ documents: data }));
  } catch (e) {
    yield put(fetchDocumentsFailure({ error: e.message }));
  }
}

/*
  Starts worker saga on latest dispatched `FETCH_DOCUMENTS_REQUEST` action.
  Allows concurrent increments.
*/
function* documentsSaga() {
  yield all([takeLatest(FETCH_DOCUMENTS_REQUEST, fetchDocumentsSaga)]);
}

export default documentsSaga;
