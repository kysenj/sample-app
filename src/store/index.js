import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';

import createRootReducer from './reducers';
import { rootSaga } from './sagas';

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// History to be used in router
export const history = createBrowserHistory();

// Root Reducer
const rootReducer = createRootReducer(history);

// Mount it on the Store
const store = createStore(
  rootReducer,
  {},
  composeWithDevTools(applyMiddleware(routerMiddleware(history), sagaMiddleware, logger))
);

// Run the saga
sagaMiddleware.run(rootSaga);

export default store;
