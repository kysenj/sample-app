import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import documentsReducer from './documents/reducer';
import topicsReducer from './topics/reducer';

const createRootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    documents: documentsReducer,
    topics: topicsReducer,
  });

export default createRootReducer;
