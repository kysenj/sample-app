import React, { Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Loader } from 'components/common';

const Documents = React.lazy(() => import(/* webpackChunkName: "app-documents" */ 'pages/Documents'));
const NotFound = React.lazy(() => import(/* webpackChunkName: "app-notfound" */ 'pages/NotFound'));

const Routes = () => (
  <Suspense fallback={<Loader />}>
    <Router>
      <Switch>
        <Route exact path="/" render={(props) => <Documents {...props} />} />
        {/**
         * NOTE: We can add more page routes here
         */}
        <Route component={NotFound} />
      </Switch>
    </Router>
  </Suspense>
);

export default Routes;
