import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchTopics } from 'store/topics/actions';
import { getTopicsSelector } from 'store/topics/selectors';
import { Chip } from 'components/common';

const DocumentTopics = ({ selectedTopicIds, onDeselectTopic }) => {
  // Dispather
  const dispatch = useDispatch();

  // Topics
  const topics = useSelector(getTopicsSelector);

  useEffect(() => {
    dispatch(fetchTopics());
  }, [dispatch]);

  const getTopicNameById = (topicId) => {
    const topic = topics.find(({ id }) => id === topicId);

    if (topic && topic.name) {
      return topic.name;
    }

    return null;
  };

  return (
    <div className="document-topics">
      {selectedTopicIds.map((topicId) => (
        <Chip key={topicId} title={getTopicNameById(topicId)} onRemove={() => onDeselectTopic(topicId)} />
      ))}
    </div>
  );
};

DocumentTopics.propTypes = {
  selectedTopicIds: PropTypes.arrayOf(PropTypes.number),
  onDeselectTopic: PropTypes.func,
};

DocumentTopics.defaultProps = {
  selectedTopicIds: [],
  onDeselectTopic: () => {},
};

export default DocumentTopics;
