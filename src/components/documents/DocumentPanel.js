import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, CardBody, CardTitle, CardSubtitle, Collapse, Button } from 'reactstrap';

const DocumentPanel = ({ document, toggle }) => {
  const [isOpenSummary, setIsOpenSummary] = useState(false);

  useEffect(() => {
    setIsOpenSummary(false);
  }, [document]);

  const toggleSummary = () => setIsOpenSummary(!isOpenSummary);

  return (
    <Card className="document-panel1">
      {document ? (
        <CardBody>
          <Button className="close" onClick={toggle}>
            <span aria-hidden="true">&times;</span>
          </Button>
          <CardTitle>Title: {document.title}</CardTitle>
          <CardSubtitle>Author: {document.agencies.map((agency) => agency.name).join(' ')}</CardSubtitle>
          <CardSubtitle>Category: {document.category}</CardSubtitle>
          <CardSubtitle>
            Published: {document.publication_date ? moment(document.publication_date).format('MM/DD/YYYY') : ''}
          </CardSubtitle>
          <Button color="primary" className="mt-4 mb-2 px-4" onClick={toggleSummary}>
            Summary
          </Button>
          <Collapse isOpen={isOpenSummary}>
            <Card>
              <CardBody className="summary">{document.summary_text}</CardBody>
            </Card>
          </Collapse>
        </CardBody>
      ) : (
        <CardBody>
          <CardSubtitle>Please select a document to see more information.</CardSubtitle>
        </CardBody>
      )}
    </Card>
  );
};

DocumentPanel.propTypes = {
  document: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    publication_date: PropTypes.string,
    agencies: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
      })
    ),
    category: PropTypes.string.isRequired,
    summary_text: PropTypes.string.isRequired,
  }),
  toggle: PropTypes.func,
};

DocumentPanel.defaultProps = {
  document: null,
  toggle: () => {},
};

export default DocumentPanel;
