import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';

const DocumentSearch = ({ searchKey, onSearchChanged }) => (
  <div className="document-search">
    <Input
      id="search"
      type="text"
      name="keyword"
      defaultValue={searchKey}
      placeholder="Search by Title"
      onKeyPress={onSearchChanged}
    />
  </div>
);

DocumentSearch.propTypes = {
  searchKey: PropTypes.string,
  onSearchChanged: PropTypes.func,
};

DocumentSearch.defaultProps = {
  searchKey: '',
  onSearchChanged: () => {},
};

export default DocumentSearch;
