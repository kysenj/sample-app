import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';

const DocumentCard = ({ document, selectedTopicIds, onSelectDocument, onSelectTopic }) => (
  <Card className="document-card" onClick={() => onSelectDocument(document)}>
    <CardBody>
      <CardTitle>{document.title}</CardTitle>
      <CardSubtitle>
        <span className="text-primary">
          {document.publication_date ? moment(document.publication_date).format('MM/DD/YYYY') : ''}
        </span>
        <span>{document.category}</span>
      </CardSubtitle>
    </CardBody>
    <CardBody className="pt-0">
      {document.topics.map((topic) => {
        const isSelectedTopic = selectedTopicIds.includes(topic.id);

        return (
          <Button
            key={topic.id}
            className="btn-topic"
            outline={!isSelectedTopic}
            color="info"
            size="sm"
            disabled={isSelectedTopic}
            onClick={(e) => onSelectTopic(e, topic.id)}
          >
            {topic.name}
          </Button>
        );
      })}
    </CardBody>
  </Card>
);

DocumentCard.propTypes = {
  document: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    publication_date: PropTypes.string,
    topics: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      })
    ).isRequired,
    category: PropTypes.string.isRequired,
  }).isRequired,
  selectedTopicIds: PropTypes.arrayOf(PropTypes.number),
  onSelectDocument: PropTypes.func.isRequired,
  onSelectTopic: PropTypes.func.isRequired,
};

DocumentCard.defaultProps = {
  selectedTopicIds: [],
};

export default DocumentCard;
