import React from 'react';
import { Spinner } from 'reactstrap';

const Loader = () => <Spinner className="loader" />;

export default Loader;
