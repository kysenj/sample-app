import React from 'react';
import PropTypes from 'prop-types';
import { Badge, Button } from 'reactstrap';

const Chip = ({ title, onRemove }) => (
  <Badge color="info" className="chip">
    {title}
    <Button className="close" onClick={onRemove}>
      <span aria-hidden="true">&times;</span>
    </Button>
  </Badge>
);
Chip.propTypes = {
  title: PropTypes.string.isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default Chip;
