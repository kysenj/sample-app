import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardTitle } from 'reactstrap';

const EmptyCard = ({ title }) => (
  <Card className="empty-card">
    <CardBody>
      <CardTitle>{title}</CardTitle>
    </CardBody>
  </Card>
);

EmptyCard.propTypes = {
  title: PropTypes.string,
};

EmptyCard.defaultProps = {
  title: '',
};

export default EmptyCard;
